<!----------------------------------------------------------------------------->

# nlb/balancer

#### Manage Network Load Balancers

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//nlb/balancer`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_nlb_balancer" {
  source  = "gitlab.com/bitservices/ingress/aws//nlb/balancer"
  vpc     = "sandpit01"
  name    = "foo-bar"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->
