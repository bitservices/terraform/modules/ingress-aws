<!----------------------------------------------------------------------------->

# nlb/listener

#### Manage Network Load Balancer listeners

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//nlb/listener`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_nlb_balancer" {
  source  = "gitlab.com/bitservices/ingress/aws//nlb/balancer"
  name    = "foo-bar"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_nlb_target_detached" {
  source  = "gitlab.com/bitservices/ingress/aws//nlb/target/detached"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
  lb_name = module.my_nlb_balancer.name
}

module "my_nlb_listener" {
  source       = "gitlab.com/bitservices/ingress/aws//nlb/listener"
  target_arn   = module.my_nlb_target_detached.arn
  balancer_arn = module.my_nlb_balancer.arn
}
```

<!----------------------------------------------------------------------------->
