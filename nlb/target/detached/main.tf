################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "lb_name" {
  type        = string
  description = "The name of the Network Load Balancer that this target group will be associated with."
}

################################################################################
# Optional Variables
################################################################################

variable "port" {
  type        = number
  default     = 443
  description = "The port that the instances will be listening for requests."
}

variable "group" {
  type        = string
  default     = "default"
  description = "The prefix of the target group. 'lb_name' is appended to this to form the name of the target group."
}

variable "protocol" {
  type        = string
  default     = "TCP"
  description = "The protocol to use for sending requests on to the instances."
}

variable "deregistration_delay" {
  type        = number
  default     = 300
  description = "The amount time to wait before changing the state of a deregistering target from draining to unused."
}

################################################################################

variable "healthcheck_port" {
  type        = number
  default     = null
  description = "The port to use to connect with the target for the health check."
}

variable "healthcheck_interval" {
  type        = number
  default     = 30
  description = "The approximate amount of time, in seconds, between health checks of an individual target."
}

variable "healthcheck_protocol" {
  type        = string
  default     = null
  description = "The protocol to use to connect with the target for the health check."
}

variable "healthcheck_threshold" {
  type        = number
  default     = 2
  description = "The number of consecutive successful/failed health check failures required before considering the target healthy/unhealthy."
}

################################################################################
# Locals
################################################################################

locals {
  name                 = format("%s-%s", var.group, var.lb_name)
  healthcheck_port     = coalesce(var.healthcheck_port, "traffic-port")
  healthcheck_protocol = coalesce(var.healthcheck_protocol, var.protocol)
}

################################################################################
# Resources
################################################################################

resource "aws_lb_target_group" "scope" {
  name                 = local.name
  port                 = var.port
  vpc_id               = data.aws_vpc.scope.id
  protocol             = var.protocol
  deregistration_delay = var.deregistration_delay

  tags = {
    VPC      = var.vpc
    Name     = local.name
    Port     = var.port
    Group    = var.group
    Owner    = var.owner
    Company  = var.company
    Balancer = var.lb_name
    Protocol = var.protocol
  }

  health_check {
    port                = local.healthcheck_port
    interval            = var.healthcheck_interval
    protocol            = local.healthcheck_protocol
    healthy_threshold   = var.healthcheck_threshold
    unhealthy_threshold = var.healthcheck_threshold
  }

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "name" {
  value = local.name
}

output "port" {
  value = var.port
}

output "group" {
  value = var.group
}

output "lb_name" {
  value = var.lb_name
}

output "protocol" {
  value = var.protocol
}

output "deregistration_delay" {
  value = var.deregistration_delay
}

################################################################################

output "healthcheck_port" {
  value = local.healthcheck_port
}

output "healthcheck_interval" {
  value = var.healthcheck_interval
}

output "healthcheck_protocol" {
  value = local.healthcheck_protocol
}

output "healthcheck_threshold" {
  value = var.healthcheck_threshold
}

################################################################################

output "arn" {
  value = aws_lb_target_group.scope.arn
}

################################################################################
