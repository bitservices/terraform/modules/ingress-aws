################################################################################
# Optional Variables
################################################################################

variable "route53_private_zone_base" {
  type        = string
  default     = "bitservices.io"
  description = "The DNS zone where private Route53 records will be created for this Application Load Balancer."
}

variable "route53_private_zone_vpc_prefix" {
  type        = bool
  default     = true
  description = "Should the name of the VPC be automatically prefixed to 'route53_private_zone_base'."
}

################################################################################
# Locals
################################################################################

locals {
  route53_private_zone_name       = format("%s%s", local.route53_private_zone_vpc_prefix, var.route53_private_zone_base)
  route53_private_zone_vpc_prefix = var.route53_private_zone_vpc_prefix ? format("%s.", var.vpc) : ""
}

################################################################################
# Data Sources
################################################################################

data "aws_route53_zone" "private" {
  count        = var.route53_private_records_ipv4_create || var.route53_private_records_ipv6_create ? 1 : 0
  name         = local.route53_private_zone_name
  vpc_id       = data.aws_vpc.scope.id
  private_zone = true
}

################################################################################
# Outputs
################################################################################

output "route53_private_zone_base" {
  value = var.route53_private_zone_base
}

output "route53_private_zone_vpc_prefix" {
  value = var.route53_private_zone_vpc_prefix
}

################################################################################

output "route53_private_zone_id" {
  value = length(data.aws_route53_zone.private) == 1 ? data.aws_route53_zone.private[0].id : null
}

output "route53_private_zone_name" {
  value = length(data.aws_route53_zone.private) == 1 ? data.aws_route53_zone.private[0].name : null
}

output "route53_private_zone_private" {
  value = length(data.aws_route53_zone.private) == 1 ? data.aws_route53_zone.private[0].private_zone : null
}

################################################################################
