################################################################################
# Modules
################################################################################

module "clb_attached" {
  source  = "../clb/attached"
  vpc     = local.vpc
  name    = "foo-bar"
  owner   = local.owner
  company = local.company

  instances_list = [
    local.instance
  ]
}

################################################################################
