<!----------------------------------------------------------------------------->

# alb/target/attached

#### Manage Application Load Balancer target groups pre-populated with [EC2] instances or [ECS] containers where requests will be directed

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//alb/target/attached`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_ec2_demand_instances" {
  source  = "gitlab.com/bitservices/compute/aws//ec2/instance/demand"
  count   = 2
  class   = format("instance-%d.foo.bar", count.index + 1)
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_alb_balancer" {
  source  = "gitlab.com/bitservices/ingress/aws//alb/balancer"
  name    = "foo-bar"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_alb_target_attached" {
  source         = "gitlab.com/bitservices/ingress/aws//alb/target/attached"
  vpc            = var.vpc
  owner          = var.owner
  company        = var.company
  lb_name        = module.my_alb_balancer.name
  instances_list = module.my_ec2_demand_instances.*.id
}
```

<!----------------------------------------------------------------------------->

[EC2]: https://aws.amazon.com/ec2
[ECS]: https://aws.amazon.com/ecs/

<!----------------------------------------------------------------------------->
