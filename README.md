<!----------------------------------------------------------------------------->

# ingress (aws)

<!----------------------------------------------------------------------------->

## Description

Manages ingress components for [AWS] such as load-balancing.

<!----------------------------------------------------------------------------->

## Modules

* [alb/balancer](alb/balancer/README.md) - Manage Application Load Balancers.
* [alb/listener](alb/listener/README.md) - Manage Application Load Balancer listeners.
* [alb/rule](alb/rule/README.md) - Manage Application Load Balancer listener rules.
* [alb/target/attached](alb/target/attached/README.md) - Manage Application Load Balancer target groups pre-populated with [EC2] instances or [ECS] containers where requests will be directed.
* [alb/target/detached](alb/target/detached/README.md) - Manage empty Application Load Balancer target groups.
* [clb/attached](clb/attached/README.md) - Manage Classic Load Balancers with [EC2] instances attached.
* [clb/detached](clb/detached/README.md) - Manage Classic Load Balancers without [EC2] instances attached.
* [nlb/balancer](nlb/balancer/README.md) - Manage Network Load Balancers.
* [nlb/listener](nlb/listener/README.md) - Manage Network Load Balancer listeners.
* [nlb/target/attached](nlb/target/attached/README.md) - Manage Network Load Balancer target groups pre-populated with [EC2] instances or [ECS] containers where requests will be directed.
* [nlb/target/detached](nlb/target/detached/README.md) - Manage empty Network Load Balancer target groups.

<!----------------------------------------------------------------------------->

[AWS]: https://aws.amazon.com/
[EC2]: https://aws.amazon.com/ec2/
[ECS]: https://aws.amazon.com/ecs/

<!----------------------------------------------------------------------------->
