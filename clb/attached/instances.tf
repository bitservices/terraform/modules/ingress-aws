################################################################################
# Required Variables
################################################################################

variable "instances_list" {
  type        = list(string)
  description = "The list of EC2 instance IDs to attach this Classic Load Balancer to."
}

################################################################################
# Resources
################################################################################

resource "aws_elb_attachment" "scope" {
  count    = length(var.instances_list)
  elb      = aws_elb.scope.name
  instance = var.instances_list[count.index]
}

################################################################################
