<!----------------------------------------------------------------------------->

# nlb/target/detached

#### Manage empty Network Load Balancer target groups

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//nlb/target/detached`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_nlb_balancer" {
  source  = "gitlab.com/bitservices/ingress/aws//nlb/balancer"
  name    = "foo-bar"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_nlb_target_detached" {
  source  = "gitlab.com/bitservices/ingress/aws//nlb/target/detached"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
  lb_name = module.my_nlb_balancer.name
}
```

<!----------------------------------------------------------------------------->
