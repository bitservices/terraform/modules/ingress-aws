################################################################################
# Optional Variables
################################################################################

variable "route53_private_records_target_health" {
  type        = bool
  default     = false
  description = "Set to 'true' if you want Route53 to determine whether to respond to DNS queries using this resource record set by checking the health of the resource record set in the private Route53 zone."
}

################################################################################

variable "route53_private_records_ipv4_type" {
  type        = string
  default     = "A"
  description = "The private IPv4 record type. This should always be 'A'."
}

variable "route53_private_records_ipv4_create" {
  type        = bool
  default     = true
  description = "Should private IPv4 DNS records be created for this Network Load Balancer."
}

################################################################################

variable "route53_private_records_ipv6_type" {
  type        = string
  default     = "AAAA"
  description = "The private IPv6 record type. This should always be 'AAAA'."
}

variable "route53_private_records_ipv6_create" {
  type        = bool
  default     = true
  description = "Should private IPv6 DNS records be created for this Network Load Balancer."
}

################################################################################
# Locals
################################################################################

locals {
  route53_private_records_suffix = join("", data.aws_route53_zone.private.*.name)
}

################################################################################
# Resources
################################################################################

resource "aws_route53_record" "private_ipv4" {
  count   = var.route53_private_records_ipv4_create ? 1 : 0
  name    = format("%s.%s", var.name, local.route53_private_records_suffix)
  type    = var.route53_private_records_ipv4_type
  zone_id = data.aws_route53_zone.private[0].id

  alias {
    name                   = aws_lb.scope.dns_name
    zone_id                = aws_lb.scope.zone_id
    evaluate_target_health = var.route53_private_records_target_health
  }
}

################################################################################

resource "aws_route53_record" "private_ipv6" {
  count   = var.route53_private_records_ipv6_create && var.enable_ipv6 ? 1 : 0
  name    = format("%s.%s", var.name, local.route53_private_records_suffix)
  type    = var.route53_private_records_ipv6_type
  zone_id = data.aws_route53_zone.private[0].id

  alias {
    name                   = aws_lb.scope.dns_name
    zone_id                = aws_lb.scope.zone_id
    evaluate_target_health = var.route53_private_records_target_health
  }
}

################################################################################
# Outputs
################################################################################

output "route53_private_records_target_health" {
  value = var.route53_private_records_target_health
}

################################################################################

output "route53_private_records_ipv4_create" {
  value = var.route53_private_records_ipv4_create
}

################################################################################

output "route53_private_records_ipv6_create" {
  value = var.route53_private_records_ipv6_create
}

################################################################################

output "route53_private_records_ipv4_name" {
  value = length(aws_route53_record.private_ipv4) == 1 ? aws_route53_record.private_ipv4[0].name : null
}

output "route53_private_records_ipv4_type" {
  value = length(aws_route53_record.private_ipv4) == 1 ? aws_route53_record.private_ipv4[0].type : null
}

################################################################################

output "route53_private_records_ipv6_name" {
  value = length(aws_route53_record.private_ipv6) == 1 ? aws_route53_record.private_ipv6[0].name : null
}

output "route53_private_records_ipv6_type" {
  value = length(aws_route53_record.private_ipv6) == 1 ? aws_route53_record.private_ipv6[0].type : null
}

################################################################################
