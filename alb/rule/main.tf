################################################################################
# Required Variables
################################################################################

variable "type" {
  type        = string
  description = "Specify if the rule is based on a URL path or the host header field."
}

variable "priority" {
  type        = number
  description = "The priority for the rule."
}

variable "conditions" {
  type        = list(string)
  description = "A list of path patterns or host headers to match."
}

variable "target_arn" {
  type        = string
  description = "The ARN of the target group to route requests to."
}

variable "listener_arn" {
  type        = string
  description = "The ARN of the listener to which to attach the rule."
}

################################################################################
# Optional Variables
################################################################################

variable "action" {
  type        = string
  default     = "forward"
  description = "The type of routing action."
}

################################################################################
# Resources
################################################################################

resource "aws_lb_listener_rule" "scope" {
  priority     = var.priority
  listener_arn = var.listener_arn

  action {
    type             = var.action
    target_group_arn = var.target_arn
  }

  condition {
    dynamic "host_header" {
      for_each = lower(var.type) == "host_header" ? tolist([var.type]) : []

      content {
        values = var.conditions
      }
    }

    dynamic "path_pattern" {
      for_each = lower(var.type) == "path_pattern" ? tolist([var.type]) : []

      content {
        values = var.conditions
      }
    }
  }
}

################################################################################
# Outputs
################################################################################

output "type" {
  value = var.type
}

output "priority" {
  value = var.priority
}

output "conditions" {
  value = var.conditions
}

output "target_arn" {
  value = var.target_arn
}

output "listener_arn" {
  value = var.listener_arn
}

################################################################################

output "action" {
  value = var.action
}

################################################################################

output "arn" {
  value = aws_lb_listener_rule.scope.arn
}

################################################################################
