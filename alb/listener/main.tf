################################################################################
# Required Variables
################################################################################

variable "balancer_arn" {
  type        = string
  description = "The ARN of the Application Load Balancer."
}

variable "target_arn" {
  type        = string
  description = "The ARN of the default target group to route requests to."
}

################################################################################
# Optional Variables
################################################################################

variable "port" {
  type        = number
  default     = null
  description = "The port on which the load balancer is listening."
}

variable "protocol" {
  type        = string
  default     = "HTTP"
  description = "The protocol for connections from clients to the load balancer."
}

variable "ssl_policy" {
  type        = string
  default     = "ELBSecurityPolicy-FS-1-2-Res-2020-10"
  description = "The name of the SSL Policy for the listener."
}

variable "default_type" {
  type        = string
  default     = "forward"
  description = "The type of routing action."
}

variable "certificate_arn" {
  type        = string
  default     = null
  description = "The ARN of the SSL server certificate. Must be specified if 'protocol' is 'HTTPS'."
}

################################################################################
# Locals
################################################################################

locals {
  port            = coalesce(var.port, var.protocol == "HTTP" ? 80 : 443)
  ssl_policy      = var.protocol == "HTTPS" ? var.ssl_policy : null
  certificate_arn = var.protocol == "HTTPS" ? var.certificate_arn : null
}

################################################################################
# Resources
################################################################################

resource "aws_lb_listener" "scope" {
  port              = local.port
  protocol          = var.protocol
  ssl_policy        = local.ssl_policy
  certificate_arn   = local.certificate_arn
  load_balancer_arn = var.balancer_arn

  default_action {
    target_group_arn = var.target_arn
    type             = var.default_type
  }
}

################################################################################
# Outputs
################################################################################

output "balancer_arn" {
  value = var.balancer_arn
}

output "target_arn" {
  value = var.target_arn
}

################################################################################

output "port" {
  value = local.port
}

output "protocol" {
  value = var.protocol
}

output "ssl_policy" {
  value = local.ssl_policy
}

output "default_type" {
  value = var.default_type
}

output "certificate_arn" {
  value = local.certificate_arn
}

################################################################################

output "arn" {
  value = aws_lb_listener.scope.arn
}

################################################################################
