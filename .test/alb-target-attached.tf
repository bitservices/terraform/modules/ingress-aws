################################################################################
# Modules
################################################################################

module "alb_target_attached" {
  source  = "../alb/target/attached"
  vpc     = local.vpc
  owner   = local.owner
  company = local.company
  lb_name = module.alb_balancer.name

  instances_list = [
    local.instance
  ]
}

################################################################################
