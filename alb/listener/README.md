<!----------------------------------------------------------------------------->

# alb/listener

#### Manage Application Load Balancer listeners

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//alb/listener`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "name"    { default = "foo-bar"                  }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_alb_balancer" {
  source      = "gitlab.com/bitservices/ingress/aws//alb/balancer"
  vpc         = var.vpc
  name        = var.name
  owner       = var.owner
  company     = var.company
}

module "my_alb_target_detached" {
  source   = "gitlab.com/bitservices/ingress/aws//alb/target/detached"
  owner    = var.owner
  vpc      = var.vpc
  company  = var.company
  alb_name = module.my_alb_balancer.name
}

module "my_alb_listener" {
  source       = "gitlab.com/bitservices/ingress/aws//alb/listener"
  target_arn   = module.my_alb_target_detached.arn
  balancer_arn = module.my_alb_balancer.arn
}
```

<!----------------------------------------------------------------------------->
