################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "lb_name" {
  type        = string
  description = "The name of the Application Load Balancer that this target group will be associated with."
}

################################################################################
# Optional Variables
################################################################################

variable "port" {
  type        = number
  default     = null
  description = "The port that the instances will be listening for requests."
}

variable "group" {
  type        = string
  default     = "default"
  description = "The prefix of the target group. 'lb_name' is appended to this to form the name of the target group."
}

variable "protocol" {
  type        = string
  default     = "HTTP"
  description = "The protocol to use for sending requests on to the instances."
}

variable "deregistration_delay" {
  type        = number
  default     = 300
  description = "The amount time to wait before changing the state of a deregistering target from draining to unused."
}

################################################################################

variable "healthcheck_path" {
  type        = string
  default     = "/"
  description = "The destination for the health check request."
}

variable "healthcheck_port" {
  type        = number
  default     = null
  description = "The port to use to connect with the target for the health check."
}

variable "healthcheck_timeout" {
  type        = number
  default     = 5
  description = "The amount of time, in seconds, during which no response means a failed health check."
}

variable "healthcheck_interval" {
  type        = number
  default     = 30
  description = "The approximate amount of time, in seconds, between health checks of an individual target."
}

variable "healthcheck_protocol" {
  type        = string
  default     = null
  description = "The protocol to use to connect with the target for the health check."
}

variable "healthcheck_match_codes" {
  type        = string
  default     = "200"
  description = "The HTTP codes to use when checking for a successful response from a target."
}

variable "healthcheck_healthy_threshold" {
  type        = number
  default     = 5
  description = "The number of consecutive health checks successes required before considering an unhealthy target healthy."
}

variable "healthcheck_unhealthy_threshold" {
  type        = number
  default     = 2
  description = "The number of consecutive health check failures required before considering the target unhealthy."
}

################################################################################

variable "stickiness_enabled" {
  type        = bool
  default     = false
  description = "Boolean to enable / disable stickiness."
}

variable "stickiness_type" {
  type        = string
  default     = "lb_cookie"
  description = "The type of sticky sessions."
}

variable "stickiness_time" {
  type        = number
  default     = 86400
  description = "The time period, in seconds, during which requests from a client should be routed to the same target."
}

################################################################################
# Locals
################################################################################

locals {
  name                 = format("%s-%s", var.group, var.lb_name)
  port                 = coalesce(var.port, var.protocol == "HTTP" ? 80 : 443)
  healthcheck_port     = coalesce(var.healthcheck_port, "traffic-port")
  healthcheck_protocol = coalesce(var.healthcheck_protocol, var.protocol)
}

################################################################################
# Resources
################################################################################

resource "aws_lb_target_group" "scope" {
  name                 = local.name
  port                 = local.port
  vpc_id               = data.aws_vpc.scope.id
  protocol             = var.protocol
  deregistration_delay = var.deregistration_delay

  tags = {
    VPC      = var.vpc
    Name     = local.name
    Port     = local.port
    Group    = var.group
    Owner    = var.owner
    Company  = var.company
    Balancer = var.lb_name
    Protocol = var.protocol
  }

  health_check {
    port                = local.healthcheck_port
    path                = var.healthcheck_path
    matcher             = var.healthcheck_match_codes
    timeout             = var.healthcheck_timeout
    interval            = var.healthcheck_interval
    protocol            = local.healthcheck_protocol
    healthy_threshold   = var.healthcheck_healthy_threshold
    unhealthy_threshold = var.healthcheck_unhealthy_threshold
  }

  stickiness {
    type            = var.stickiness_type
    enabled         = var.stickiness_enabled
    cookie_duration = var.stickiness_time
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "name" {
  value = local.name
}

output "port" {
  value = local.port
}

output "group" {
  value = var.group
}

output "lb_name" {
  value = var.lb_name
}

output "protocol" {
  value = var.protocol
}

output "deregistration_delay" {
  value = var.deregistration_delay
}

################################################################################

output "healthcheck_path" {
  value = var.healthcheck_path
}

output "healthcheck_port" {
  value = local.healthcheck_port
}

output "healthcheck_timeout" {
  value = var.healthcheck_timeout
}

output "healthcheck_interval" {
  value = var.healthcheck_interval
}

output "healthcheck_protocol" {
  value = local.healthcheck_protocol
}

output "healthcheck_match_codes" {
  value = var.healthcheck_match_codes
}

output "healthcheck_healthy_threshold" {
  value = var.healthcheck_healthy_threshold
}

output "healthcheck_unhealthy_threshold" {
  value = var.healthcheck_unhealthy_threshold
}

################################################################################

output "stickiness_time" {
  value = var.stickiness_time
}

output "stickiness_type" {
  value = var.stickiness_type
}

output "stickiness_enabled" {
  value = var.stickiness_enabled
}

################################################################################

output "arn" {
  value = aws_lb_target_group.scope.arn
}

################################################################################
