<!----------------------------------------------------------------------------->

# clb/attached

#### Manage Classic Load Balancers and associated [Route53] record with [EC2] instances attached

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//clb/attached`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_ec2_demand_instance_1" {
  source  = "gitlab.com/bitservices/compute/aws//ec2/instance/demand"
  class   = "instance1.foo.bar"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_ec2_demand_instance_2" {
  source  = "gitlab.com/bitservices/compute/aws//ec2/instance/demand"
  class   = "instance2.foo.bar"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_clb_attached" {
  source         = "gitlab.com/bitservices/ingress/aws//clb/attached"
  name           = "foo-bar"
  vpc            = var.vpc
  owner          = var.owner
  company        = var.company
  instances_list = tolist([ module.my_ec2_demand_instance_1.id, module.my_ec2_demand_instance_2.id ])
}
```

<!----------------------------------------------------------------------------->

[EC2]:     https://aws.amazon.com/ec2
[Route53]: https://aws.amazon.com/route53

<!----------------------------------------------------------------------------->
