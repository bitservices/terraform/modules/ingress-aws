################################################################################
# Required Variables
################################################################################

variable "balancer_arn" {
  type        = string
  description = "The ARN of the Network Load Balancer."
}

variable "target_arn" {
  type        = string
  description = "The ARN of the default target group to route requests to."
}

################################################################################
# Optional Variables
################################################################################

variable "port" {
  type        = number
  default     = 443
  description = "The port on which the load balancer is listening."
}

variable "protocol" {
  type        = string
  default     = "TCP"
  description = "The protocol for connections from clients to the load balancer."
}

variable "default_type" {
  type        = string
  default     = "forward"
  description = "The type of routing action."
}

################################################################################
# Resources
################################################################################

resource "aws_lb_listener" "scope" {
  port              = var.port
  protocol          = var.protocol
  load_balancer_arn = var.balancer_arn

  default_action {
    target_group_arn = var.target_arn
    type             = var.default_type
  }
}

################################################################################
# Outputs
################################################################################

output "balancer_arn" {
  value = var.balancer_arn
}

output "target_arn" {
  value = var.target_arn
}

################################################################################

output "port" {
  value = var.port
}

output "protocol" {
  value = var.protocol
}

output "default_type" {
  value = var.default_type
}

################################################################################

output "arn" {
  value = aws_lb_listener.scope.arn
}

################################################################################
