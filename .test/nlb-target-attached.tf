################################################################################
# Modules
################################################################################

module "nlb_target_attached" {
  source  = "../nlb/target/attached"
  vpc     = local.vpc
  owner   = local.owner
  company = local.company
  lb_name = module.nlb_balancer.name

  instances_list = [
    local.instance
  ]
}

################################################################################
