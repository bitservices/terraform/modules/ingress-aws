################################################################################
# Modules
################################################################################

module "alb_target_detached" {
  source  = "../alb/target/detached"
  vpc     = local.vpc
  owner   = local.owner
  company = local.company
  lb_name = module.alb_balancer.name
}

################################################################################
