################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of this Classic Load Balancer."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "port" {
  type        = number
  default     = null
  description = "The network port the Classic Load Balancer operates on."
}

variable "internal" {
  type        = bool
  default     = false
  description = "If 'true', the Classic Load Balancer will be internal only."
}

variable "protocol" {
  type        = string
  default     = "TCP"
  description = "The network protocol the Classic Load Balancer operates on."
}

variable "certificate" {
  type        = string
  default     = null
  description = "The ARN of an SSL certificate you have uploaded to AWS IAM. Must be specified when 'listener_protocol' is 'HTTPS' or 'SSL'."
}

variable "idle_timeout" {
  type        = number
  default     = 60
  description = "The time in seconds that the connection is allowed to be idle."
}

variable "connection_draining" {
  type        = bool
  default     = false
  description = "Boolean to enable connection draining."
}

variable "cross_zone_load_balancing" {
  type        = bool
  default     = true
  description = "Enable cross-zone load balancing."
}

variable "connection_draining_timeout" {
  type        = number
  default     = 300
  description = "The time in seconds to allow for connections to drain."
}

################################################################################

variable "health_check_port" {
  type        = number
  default     = null
  description = "The port of the target of the check. Defaults to 'target_port'."
}

variable "health_check_timeout" {
  type        = number
  default     = 5
  description = "The length of time in seconds before the check times out."
}

variable "health_check_interval" {
  type        = number
  default     = 15
  description = "The interval in seconds between checks."
}

variable "health_check_location" {
  type        = string
  default     = "/"
  description = "The path of the target of the check. Ignored unless 'health_check_protocol' is 'HTTP' or 'HTTPS'."
}

variable "health_check_protocol" {
  type        = string
  default     = null
  description = "The protocol of the target of the check. Defaults to 'target_protocol'."
}

variable "health_check_healthy_threshold" {
  type        = number
  default     = 4
  description = "The number of successful checks before an EC2 instance is declared healthy."
}

variable "health_check_unhealthy_threshold" {
  type        = number
  default     = 2
  description = "The number of unsuccessful checks before the EC2 instance is declared unhealthy."
}

################################################################################

variable "listener_port" {
  type        = number
  default     = null
  description = "The port to listen on for the Classic Load Balancer. Defaults to 'port'."
}

variable "listener_protocol" {
  type        = string
  default     = null
  description = "The protocol to listen on for the Classic Load Balancer. Defaults to 'protocol'."
}

################################################################################

variable "target_port" {
  type        = number
  default     = null
  description = "The port on the EC2 instances to route to. Defaults to 'port'."
}

variable "target_protocol" {
  type        = string
  default     = null
  description = "The protocol to use to the EC2 instances. Defaults to 'protocol'."
}

################################################################################
# Locals
################################################################################

locals {
  port                  = coalesce(var.port, var.protocol == "HTTP" ? 80 : 443)
  target_port           = coalesce(var.target_port, local.port)
  listener_port         = coalesce(var.listener_port, local.port)
  target_protocol       = coalesce(var.target_protocol, var.protocol)
  listener_protocol     = coalesce(var.listener_protocol, var.protocol)
  health_check_port     = coalesce(var.health_check_port, local.target_port)
  health_check_target   = format("%s:%s%s", local.health_check_protocol, local.health_check_port, local.health_check_location)
  health_check_location = contains(tolist(["HTTP", "HTTPS"]), local.health_check_protocol) ? var.health_check_location : ""
  health_check_protocol = coalesce(var.health_check_protocol, local.target_protocol)
}

################################################################################
# Resources
################################################################################

resource "aws_elb" "scope" {
  name                        = var.name
  subnets                     = local.subnet_ids
  internal                    = var.internal
  idle_timeout                = var.idle_timeout
  security_groups             = concat(var.security_group_ids, data.aws_security_group.scope.*.id)
  connection_draining         = var.connection_draining
  cross_zone_load_balancing   = var.cross_zone_load_balancing
  connection_draining_timeout = var.connection_draining_timeout

  tags = {
    VPC     = var.vpc
    Name    = var.name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }

  listener {
    lb_port            = local.listener_port
    lb_protocol        = local.listener_protocol
    instance_port      = local.target_port
    instance_protocol  = local.target_protocol
    ssl_certificate_id = var.certificate
  }

  health_check {
    target              = local.health_check_target
    timeout             = var.health_check_timeout
    interval            = var.health_check_interval
    healthy_threshold   = var.health_check_healthy_threshold
    unhealthy_threshold = var.health_check_unhealthy_threshold
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "internal" {
  value = var.internal
}

output "certificate" {
  value = var.certificate
}

output "idle_timeout" {
  value = var.idle_timeout
}

output "connection_draining" {
  value = var.connection_draining
}

output "cross_zone_load_balancing" {
  value = var.cross_zone_load_balancing
}

output "connection_draining_timeout" {
  value = var.connection_draining_timeout
}

################################################################################

output "listener_port" {
  value = local.listener_port
}

output "listener_protocol" {
  value = local.listener_protocol
}

################################################################################

output "target_port" {
  value = local.target_port
}

output "target_protocol" {
  value = local.target_protocol
}

################################################################################

output "health_check_port" {
  value = local.health_check_port
}

output "health_check_target" {
  value = local.health_check_target
}

output "health_check_timeout" {
  value = var.health_check_timeout
}

output "health_check_interval" {
  value = var.health_check_interval
}

output "health_check_location" {
  value = var.health_check_location
}

output "health_check_protocol" {
  value = local.health_check_protocol
}

output "health_check_healthy_threshold" {
  value = var.health_check_healthy_threshold
}

output "health_check_unhealthy_threshold" {
  value = var.health_check_unhealthy_threshold
}

################################################################################

output "name" {
  value = aws_elb.scope.name
}

output "zone_id" {
  value = aws_elb.scope.zone_id
}

output "dns_name" {
  value = aws_elb.scope.dns_name
}

output "instances_list" {
  value = aws_elb.scope.instances
}

output "instances_count" {
  value = length(aws_elb.scope.instances)
}

output "security_group_id" {
  value = aws_elb.scope.source_security_group_id
}

################################################################################
