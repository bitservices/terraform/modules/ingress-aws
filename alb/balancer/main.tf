################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of this Application Load Balancer."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The project/business unit to which this asset belongs."
}

################################################################################
# Optional Variables
################################################################################

variable "type" {
  type        = string
  default     = "application"
  description = "The type of the Application Load Balancer. Must always be set to 'application'."
}

variable "internal" {
  type        = bool
  default     = false
  description = "If 'true' the Application Load Balancer will be internal-only."
}

variable "enable_ipv6" {
  type        = bool
  default     = true
  description = "If 'true' IPv6 will be supported by the Application Load Balancer. Ignored if 'internal' is 'true' in which case IPv6 is disabled."
}

variable "enable_http2" {
  type        = bool
  default     = true
  description = "If 'true' HTTP/2 will be enabled."
}

variable "idle_timeout" {
  type        = number
  default     = 60
  description = "The time in seconds that idle connections will be dropped."
}

variable "disable_api_termination" {
  type        = bool
  default     = false
  description = "If 'true' the Application Load Balancer will be protected against accidental and API termination."
}

################################################################################
# Locals
################################################################################

locals {
  enable_ipv6 = var.internal ? false : var.enable_ipv6
}

################################################################################
# Resources
################################################################################

resource "aws_lb" "scope" {
  name                       = var.name
  subnets                    = local.subnet_ids
  internal                   = var.internal
  enable_http2               = var.enable_http2
  idle_timeout               = var.idle_timeout
  ip_address_type            = local.enable_ipv6 ? "dualstack" : "ipv4"
  load_balancer_type         = var.type
  security_groups            = concat(var.security_group_ids, data.aws_security_group.scope.*.id)
  enable_deletion_protection = var.disable_api_termination

  tags = {
    VPC      = var.vpc
    IPv6     = local.enable_ipv6
    Name     = var.name
    Type     = var.type
    Owner    = var.owner
    Region   = data.aws_region.scope.name
    Company  = var.company
    Internal = var.internal
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "type" {
  value = var.type
}

output "internal" {
  value = var.internal
}

output "enable_ipv6" {
  value = local.enable_ipv6
}

output "enable_http2" {
  value = var.enable_http2
}

output "idle_timeout" {
  value = var.idle_timeout
}

output "disable_api_termination" {
  value = var.disable_api_termination
}

################################################################################

output "arn" {
  value = aws_lb.scope.arn
}

output "name" {
  value = aws_lb.scope.name
}

output "zone_id" {
  value = aws_lb.scope.zone_id
}

output "dns_name" {
  value = aws_lb.scope.dns_name
}

################################################################################
