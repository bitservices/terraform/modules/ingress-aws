<!----------------------------------------------------------------------------->

# alb/rule

#### Manage Application Load Balancer listener rules

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//alb/rule`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "name"    { default = "foo-bar"                  }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_alb_balancer" {
  source  = "gitlab.com/bitservices/ingress/aws//alb/balancer"
  vpc     = var.vpc
  name    = var.name
  owner   = var.owner
  company = var.company
}

module "my_alb_target_default" {
  source   = "gitlab.com/bitservices/ingress/aws//alb/target/detached"
  vpc      = var.vpc
  owner    = var.owner
  company  = var.company
  alb_name = module.my_alb_balancer.name
}

module "my_alb_target_special" {
  source   = "gitlab.com/bitservices/ingress/aws//alb/target/detached"
  group    = "special"
  vpc      = var.vpc
  owner    = var.owner
  company  = var.company
  alb_name = module.my_alb_balancer.name
}

module "my_alb_listener" {
  source       = "gitlab.com/bitservices/ingress/aws//alb/listener"
  target_arn   = module.my_alb_target_default.arn
  balancer_arn = module.my_alb_balancer.arn
}

module "my_alb_rule" {
  source       = "gitlab.com/bitservices/ingress/aws//alb/rule"
  type         = "path-pattern"
  priority     = 100
  target_arn   = module.my_alb_target_special.arn
  listener_arn = module.my_alb_listener.arn

  conditions = [
    "/special"
  ]
}
```

<!------------------------------------------------------------------------------------------------->
