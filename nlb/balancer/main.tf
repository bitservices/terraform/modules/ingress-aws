################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of this Network Load Balancer."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "type" {
  type        = string
  default     = "network"
  description = "The type of the Network Load Balancer. Must always be set to 'network'."
}

variable "internal" {
  type        = bool
  default     = false
  description = "If 'true' the Network Load Balancer will be internal-only."
}

variable "enable_ipv6" {
  type        = bool
  default     = false
  description = "If 'true' IPv6 will be supported by the Network Load Balancer. Not currently supported."
}

variable "disable_api_termination" {
  type        = bool
  default     = false
  description = "If 'true' the Network Load Balancer will be protected against accidental and API termination."
}

variable "cross_zone_load_balancing" {
  type        = bool
  default     = true
  description = "If 'true' cross-zone load balancing of the Network Load Balancer will be enabled."
}

################################################################################
# Resources
################################################################################

resource "aws_lb" "scope" {
  name                             = var.name
  subnets                          = local.subnet_ids
  internal                         = var.internal
  ip_address_type                  = var.enable_ipv6 ? "dualstack" : "ipv4"
  load_balancer_type               = var.type
  enable_deletion_protection       = var.disable_api_termination
  enable_cross_zone_load_balancing = var.cross_zone_load_balancing

  tags = {
    VPC      = var.vpc
    IPv6     = var.enable_ipv6
    Name     = var.name
    Type     = var.type
    Owner    = var.owner
    Region   = data.aws_region.scope.name
    Company  = var.company
    Internal = var.internal
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "type" {
  value = var.type
}

output "internal" {
  value = var.internal
}

output "enable_ipv6" {
  value = var.enable_ipv6
}

output "disable_api_termination" {
  value = var.disable_api_termination
}

output "cross_zone_load_balancing" {
  value = var.cross_zone_load_balancing
}

################################################################################

output "arn" {
  value = aws_lb.scope.arn
}

output "name" {
  value = aws_lb.scope.name
}

output "zone_id" {
  value = aws_lb.scope.zone_id
}

output "dns_name" {
  value = aws_lb.scope.dns_name
}

################################################################################
