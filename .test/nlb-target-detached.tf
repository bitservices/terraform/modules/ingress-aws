################################################################################
# Modules
################################################################################

module "nlb_target_detached" {
  source  = "../nlb/target/detached"
  vpc     = local.vpc
  owner   = local.owner
  company = local.company
  lb_name = module.nlb_balancer.name
}

################################################################################
