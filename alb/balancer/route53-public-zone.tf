################################################################################
# Optional Variables
################################################################################

variable "route53_public_zone_base" {
  type        = string
  default     = "bitservices.io"
  description = "The DNS zone where public Route53 records will be created for this Application Load Balancer."
}

variable "route53_public_zone_vpc_prefix" {
  type        = bool
  default     = true
  description = "Should the name of the VPC be automatically prefixed to 'route53_public_zone_base'."
}

################################################################################
# Locals
################################################################################

locals {
  route53_public_zone_name       = format("%s%s", local.route53_public_zone_vpc_prefix, var.route53_public_zone_base)
  route53_public_zone_vpc_prefix = var.route53_public_zone_vpc_prefix ? format("%s.", var.vpc) : ""
}

################################################################################
# Data Sources
################################################################################

data "aws_route53_zone" "public" {
  count        = var.route53_public_records_ipv4_create || var.route53_public_records_ipv6_create ? 1 : 0
  name         = local.route53_public_zone_name
  private_zone = false
}

################################################################################
# Outputs
################################################################################

output "route53_public_zone_base" {
  value = var.route53_public_zone_base
}

output "route53_public_zone_vpc_prefix" {
  value = var.route53_public_zone_vpc_prefix
}

################################################################################

output "route53_public_zone_id" {
  value = length(data.aws_route53_zone.public) == 1 ? data.aws_route53_zone.public[0].id : null
}

output "route53_public_zone_name" {
  value = length(data.aws_route53_zone.public) == 1 ? data.aws_route53_zone.public[0].name : null
}

output "route53_public_zone_private" {
  value = length(data.aws_route53_zone.public) == 1 ? data.aws_route53_zone.public[0].private_zone : null
}

################################################################################
