<!----------------------------------------------------------------------------->

# alb/target/detached

#### Manage empty Application Load Balancer target groups

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//alb/target/detached`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_alb_balancer" {
  source  = "gitlab.com/bitservices/ingress/aws//alb/balancer"
  name    = "foo-bar"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_alb_target_detached" {
  source  = "gitlab.com/bitservices/ingress/aws//alb/target/detached"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
  lb_name = module.my_alb_balancer.name
}
```

<!----------------------------------------------------------------------------->
