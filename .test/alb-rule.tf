################################################################################
# Modules
################################################################################

module "alb_rule" {
  source       = "../alb/rule"
  type         = "path-pattern"
  priority     = 100
  target_arn   = module.alb_target_attached.arn
  listener_arn = module.alb_listener.arn

  conditions = [
    "/special"
  ]
}

################################################################################
