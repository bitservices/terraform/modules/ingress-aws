<!----------------------------------------------------------------------------->

# alb/balancer

#### Manage Application Load Balancers

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//alb/balancer`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_alb_balancer" {
  source  = "gitlab.com/bitservices/ingress/aws//alb/balancer"
  vpc     = "sandpit01"
  name    = "foo-bar"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->
