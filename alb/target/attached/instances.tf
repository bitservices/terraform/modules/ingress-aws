################################################################################
# Required Variables
################################################################################

variable "instances_list" {
  type        = list(string)
  description = "The list of instance IDs to attach this target group to."
}

################################################################################
# Resources
################################################################################

resource "aws_lb_target_group_attachment" "scope" {
  count            = length(var.instances_list)
  target_id        = var.instances_list[count.index]
  target_group_arn = aws_lb_target_group.scope.arn
}

################################################################################
# Outputs
################################################################################

output "instances_list" {
  value = var.instances_list
}

output "instances_count" {
  value = length(var.instances_list)
}

################################################################################
