<!----------------------------------------------------------------------------->

# clb/detached

#### Manage Classic Load Balancers and associated [Route53] record without [EC2] instances attached

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/aws//clb/detached`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_clb_detached" {
  source  = "gitlab.com/bitservices/ingress/aws//clb/detached"
  vpc     = "sandpit01"
  name    = "foo-bar"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[EC2]:     https://aws.amazon.com/ec2
[Route53]: https://aws.amazon.com/route53

<!----------------------------------------------------------------------------->
